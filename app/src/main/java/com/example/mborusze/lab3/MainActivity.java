package com.example.mborusze.lab3;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public static final String SOUND_ID = "sound_id";
    public static final String SOUND_ID_OTHER = "sound id";
    public static final int BUTTON_REQUEST = 1;
    private int current_sound = 0;
    private int current_sound_2 = 1;
    private int current_state = 0;

    private MediaPlayer backgroundPlayer;
    private MediaPlayer buttonPlayer;
    static public Uri[] sounds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sounds = setSounds();
        buttonPlayer = new MediaPlayer();

        final ImageButton imButton =
                (ImageButton)findViewById(R.id.face_button);
        imButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int random_color = 0xff000000;
                for (int i = 0; i < 3; i++) {
                    int component_color = (int)(255 * Math.random());

                    component_color <<= (8*i);
                    random_color |= component_color;
                }
                imButton.setColorFilter(random_color);


                buttonPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                buttonPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        backgroundPlayer.pause();
                        mp.start();
                    }
                });
                buttonPlayer.reset();
                try {
                    buttonPlayer.setDataSource(getApplicationContext(),sounds[current_sound]);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                buttonPlayer.prepareAsync();
            }
        });
        imButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                current_state = 1;
                Intent soundPick = new
                        Intent(getApplicationContext(),SecondActivity.class);
                soundPick.putExtra(SOUND_ID,current_sound);
                soundPick.putExtra(SOUND_ID_OTHER,current_sound_2);
                startActivityForResult(soundPick, BUTTON_REQUEST);
                return true;
            }
        });


    final ImageButton imButton2 =
            (ImageButton)findViewById(R.id.face_button2);
    imButton2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            int random_color = 0xff000000;
            for (int i = 0; i < 3; i++) {
                int component_color = (int)(255 * Math.random());

                component_color <<= (8*i);
                random_color |= component_color;
            }
            imButton2.setColorFilter(random_color);

            buttonPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            buttonPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    backgroundPlayer.pause();
                    mp.start();
                }
            });
            buttonPlayer.reset();
            try {
                buttonPlayer.setDataSource(getApplicationContext(),sounds[current_sound_2]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            buttonPlayer.prepareAsync();
        }
    });
    imButton2.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            current_state = 2;
            Intent soundPick = new
                    Intent(getApplicationContext(),SecondActivity.class);
            soundPick.putExtra(SOUND_ID,current_sound_2);
            soundPick.putExtra(SOUND_ID_OTHER,current_sound);
            startActivityForResult(soundPick, BUTTON_REQUEST);
            return true;
        }
    });
}
    private Uri[] setSounds(){
        sounds = new Uri[5];
        sounds[0] = Uri.parse("android.resource://" + getPackageName() + "/" +
                R.raw.ringd);
        sounds[1] = Uri.parse("android.resource://" + getPackageName() + "/" +
                R.raw.ring01);
        sounds[2] = Uri.parse("android.resource://" + getPackageName() + "/" +
                R.raw.ring02);
        sounds[3] = Uri.parse("android.resource://" + getPackageName() + "/" +
                R.raw.ring03);
        sounds[4] = Uri.parse("android.resource://" + getPackageName() + "/" +
                R.raw.ring04);
        return sounds;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent
            data) {
        if (resultCode == RESULT_OK) {
            if(requestCode == BUTTON_REQUEST)
            {
                if(current_state==1)
                current_sound = data.getIntExtra(SOUND_ID,0);
                else
                current_sound_2 = data.getIntExtra(SOUND_ID,0);
            }
        }
        else if(resultCode == RESULT_CANCELED){
            Toast.makeText(getApplicationContext(),getText(R.string.back_message),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        backgroundPlayer.pause();
        buttonPlayer.pause();
    }
    @Override
    protected void onResume() {
        super.onResume();

        backgroundPlayer = MediaPlayer.create(this, R.raw.mario);

        backgroundPlayer.setOnPreparedListener(new
                                                       MediaPlayer.OnPreparedListener() {
                                                           @Override
                                                           public void onPrepared(MediaPlayer mp) {

                                                               mp.setLooping(true);

                                                               mp.start();
                                                           }
                                                       });
    }
    @Override
    protected void onStop(){
        super.onStop();
        backgroundPlayer.release();
    }


}
