package com.example.mborusze.lab3;

import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity {

    private int selected_sound = 0;
    private ArrayList<RadioButton> radioButtonList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        setRadioButtonList();

        Intent received_intent = getIntent();
        Integer sound_id =
                        received_intent.getIntExtra(MainActivity.SOUND_ID,0);
        Integer sound_id_other =
                        received_intent.getIntExtra(MainActivity.SOUND_ID_OTHER,0);

        grayedRadioButton(sound_id_other);

        TextView txV = (TextView)findViewById(R.id.current_sound_text);
        txV.setText(getText(R.string.current_sound_str) +
                sound_id.toString());
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        if (checked) {
            switch (view.getId()){

                case R.id.sound1: selected_sound = 0;break;
                case R.id.sound2: selected_sound = 1;break;
                case R.id.sound3: selected_sound = 2;break;
                case R.id.sound4: selected_sound = 3;break;
                case R.id.sound5: selected_sound = 4;break;
            }
        }
    }

    public void setSoundClick(View v){
        Intent data = new Intent();
        data.putExtra(MainActivity.SOUND_ID,selected_sound);
        setResult(RESULT_OK, data);
        finish();
    }

    private void setRadioButtonList() {
        radioButtonList = new ArrayList<>();
        radioButtonList.add((RadioButton) findViewById(R.id.sound1));
        radioButtonList.add((RadioButton) findViewById(R.id.sound2));
        radioButtonList.add((RadioButton) findViewById(R.id.sound3));
        radioButtonList.add((RadioButton) findViewById(R.id.sound4));
        radioButtonList.add((RadioButton) findViewById(R.id.sound5));
    }

    private void grayedRadioButton(int id){
        unGrayedAllButtons();
        radioButtonList.get(id).setEnabled(false);
    }

    private void unGrayedAllButtons(){
        for (RadioButton rb : radioButtonList
             ) {
            rb.setEnabled(true);
        }
    }
}
